package it.unibo;

import org.junit.Test;
import org.slf4j.Logger;

import static org.junit.Assert.assertEquals;
import static org.slf4j.LoggerFactory.getLogger;

public class HelloTest {
    private static final Logger log = getLogger(HelloTest.class);

    @Test
    public void testGetMessage() {
        final Hello hello = new Hello();
        final String message = hello.getMessage();
        if (log.isDebugEnabled())
            log.debug("message={}", message);
        assertEquals("Hello World!", message);
    }
}