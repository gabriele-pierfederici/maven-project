package it.unibo;

public class Hello {
    private static final String helloWorld = "Hello World!";

    private final String message;

    private Hello(final String message) {
        this.message = message;
    }

    public Hello() {
        this(helloWorld);
    }

    public String getMessage() {
        return message;
    }
}